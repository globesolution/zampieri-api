<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class adminController extends Controller {

	public function callBackMSG($status, $msg)
	{
		$callBack = array(
					'status' => $status,
					'msg'    => $msg
				);

    	return json_encode($callBack);
	}

	public function listarPagamentos(Request $request){

		$rules = array(
    		'usuario_id'       => 'required|numeric',
    		'statusPagamento'  => 'required|numeric'
    	);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{

			
			if($this->checkAdmin($request->usuario_id, $request->senha)){

				$pagamentos['pagamentos'] = \App\tbl_pagamentos::where('status', $request->statusPagamento)->get();
				$pagamentos['status'] = 200;
				return json_encode($pagamentos);

			}else{
				$error = array(
					'status' => 404,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

    			return json_encode($error);
			}

		}
	}

	public function salvarPagamentos(Request $request){


		$rules = array(
    		'usuario_id'         => 'required|numeric',
    		'statusPagamento'    => 'required|numeric',
    		'id_pagamento'       => 'required|numeric'
    	);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{

			if($this->checkAdmin($request->usuario_id, $request->senha)){
				$pagamentos = \App\tbl_pagamentos::where('id', $request->id_pagamento)->first();

				$pagamentos->status = $request->statusPagamento;

				$pagamentos->save();

				$success = array(
					'status' => 200,
					'msg'    => 'Salvo com successo.'
				);

	    		return json_encode($success);
	    	}else{

	    		$error = array(
					'status' => 404,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

    			return json_encode($error);

	    	}

		}


	}


	public function checkAdmin($usuario_id, $usuario_senha){
		$checkUserAdmin = \App\tbl_usuarios::where('id', $usuario_id)
			->where('senha', $usuario_senha)
			->where('type_user', 2)
			->get();
		if(count($checkUserAdmin) == 1)
			return true;
		else
			return false;
	}	


	public function listarIndicacoes(Request $request){
		$rules = array(
    		'usuario_id'       => 'required|numeric',
    		'statusIndicacao'  => 'required|numeric'
    	);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{

			
			if($this->checkAdmin($request->usuario_id, $request->senha)){
				if($request->tipo_indicacao == 1){

					$indicacoesInquilinos['indicacoes'] = \App\tbl_indicacoesInquilinos::where('status', $request->statusIndicacao)->get();
					$indicacoesInquilinos['status'] = 200;

					return json_encode($indicacoesInquilinos);

				}else if($request->tipo_indicacao == 2){

					$indicacoesImoveis['indicacoes'] = \App\tbl_indicacoesImoveis::where('status', $request->statusIndicacao)->get();
					$indicacoesImoveis['status'] = 200;

					return json_encode($indicacoesImoveis);

				}else{
					$error = array(
						'status' => 404,
						'msg'    => 'Parece que algo está errado, tente novamente'
					);

    				return json_encode($error);
				}
			}else{
				$error = array(
					'status' => 404,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

    			return json_encode($error);
			}

		}

	}
	public function salvarIndicacao(Request $request){


		$rules = array(
    		'usuario_id'         => 'required|numeric',
    		'statusIndicacao'    => 'required|numeric',
    		'id_indicacao'       => 'required|numeric',
    		'tipo_indicacao'     => 'required|numeric',
    		'creditar'           => 'required'
    	);

		$validator = \Validator::make(\Input::all(), $rules);

		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{

			if($this->checkAdmin($request->usuario_id, $request->senha))
			{	
				if($request->tipo_indicacao == 1)
				{
					$indicacao = \App\tbl_indicacoesInquilinos::where('id', $request->id_indicacao)->first();

					$indicacao->status = $request->statusIndicacao;

					if($request->statusIndicacao == 2 && $request->creditar > 0){
						
						$usuarioCreditado = \App\tbl_usuarios::where('id', $indicacao->usuario_id)->first();

						$usuarioCreditado->saldo = $usuarioCreditado->saldo+$request->creditar;

		    		

						$registerExtrato = new \App\tbl_extrato;
						$registerExtrato->usuario_id = $indicacao->usuario_id;
						$registerExtrato->tipo_entrada = 2;
						$registerExtrato->valor = $request->creditar;
						$registerExtrato->save();
						$usuarioCreditado->save();
						
					}

					$indicacao->save();

					$success = array(
						'status' => 200,
						'msg'    => 'Salvo com successo.'
					);

		    		return json_encode($success);
		    	}
		    	else
		    	 if($request->tipo_indicacao == 2)
		    	 {

					$indicacao = \App\tbl_indicacoesImoveis::where('id', $request->id_indicacao)->first();
					$indicacao->status = $request->statusIndicacao;

					if($request->statusIndicacao == 2 && $request->creditar == "true"){
						
						$usuarioCreditado = \App\tbl_usuarios::where('id', $indicacao->usuario_id)->first();

						$usuarioCreditado->saldo = $usuarioCreditado->saldo+20;

		    		

						$registerExtrato = new \App\tbl_extrato;
						$registerExtrato->usuario_id = $indicacao->usuario_id;
						$registerExtrato->tipo_entrada = 2;
						$registerExtrato->valor = 20;
						$registerExtrato->save();
						$usuarioCreditado->save();
						
					}
					$indicacao->save();

					$success = array(
						'status' => 200,
						'msg'    => 'Salvo com successo.'
					);

		    		return json_encode($success);

				}
				else
				{
					$error = array(
						'status' => 404,
						'msg'    => 'Parece que algo está errado, tente novamente'
					);

    				return json_encode($error);
				}
			}
			else
			{
				
				$error = array(
					'status' => 404,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

    			return json_encode($error);
			}

		}

	}



	public function numberIndicacoes(Request $request){
		$rules = array(
    		'usuario_id'         => 'required|numeric'
    	);
		$validator = \Validator::make(\Input::all(), $rules);
		if($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			if($this->checkAdmin($request->usuario_id, $request->senha)){	

				$retorno['number_indicacao'] = array(
					'inquilinos'    => count(\App\tbl_indicacoesInquilinos::where('status', 1)->get()),
					'imoveis'		=> count(\App\tbl_indicacoesImoveis::where('status', 1)->get())
				);
				$retorno['pagamentos'] = count(\App\tbl_pagamentos::where('status', 1)->get());
				$retorno['status'] = 200;

				return json_encode($retorno);

			}else{
				$error = array(
					'status' => 404,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

    			return json_encode($error);
			}

		}
	}

	public function newMsgNovidade(Request $request){

		$rules = array(
    		'usuario_id'       => 'required|numeric'
    	);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => $validator->errors()->all()
			);

    		return json_encode($error);
		}else{

			
			if($this->checkAdmin($request->usuario_id, $request->senha)){

				$updateAllForStatusFalse = \App\tbl_msgnovidades::where('id','>',0)->where('tipo_msg','=',0)->update(['status' => 0]);

				$msgCreate = new \App\tbl_msgnovidades;
				$msgCreate->msgNovidade = $request->msgData;
				$msgCreate->usuario_id = $request->usuario_id;
				$msgCreate->tipo_msg = 0;
				$msgCreate->status = 1;
				$msgCreate->save();

				$newMsg['msg'] = "Alterado com sucesso.";
				$newMsg['status'] = 200;
				return json_encode($newMsg);
				
			}else{
				$error = array(
					'status' => 404,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

    			return json_encode($error);
			}

		}
	}

	public function sendNewMsg(Request $request)
	{
		$rules = array(
    		'usuario_id'    => 'required|numeric|min:1',
    		'msg'           => 'required'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			
			if($this->checkAdmin($request->usuario_id, $request->senha)){
				$msg = new \App\tbl_suportechat;
				$msg->msg = $request->msg;
				$msg->enviador_id = $request->enviador_id;
				$msg->receptor_id = $request->receptor_id;
				$msg->tipo_user = 2;
				$msg->lida = 1;
				$msg->save();
				$success = array(
						'status' => 200,
						'msg'    => 'Cadastro realizado com sucesso.'
					);
				return json_encode($success);
			}
		}	
	}

	public function getMsgsToList(Request $request)
	{
		$rules = array(
    		'usuario_id'    => 'required|numeric|min:1'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			if($this->checkAdmin($request->usuario_id, $request->senha)){

				$msgs['tickets'] = \App\tbl_suportechat::where('receptor_id', -9)->groupBy('enviador_id')->get();
				for ($i=0; $i <count($msgs['tickets']) ; $i++) { 
					$getName = \App\tbl_usuarios::select('nome_completo')->where('id', $msgs['tickets'][$i]['enviador_id'])->first();
					$msgs['tickets'][$i]['nome_completo'] = $getName->nome_completo;
				}

				$msgs['status'] = 200;
			}
			return json_encode($msgs);
		}	
	}

	public function getMsgsBySender(Request $request)
	{
		$rules = array(
    		'usuario_id'    => 'required|numeric|min:1',
    		'enviador_id'   => 'required|numeric|min:1'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			if($this->checkAdmin($request->usuario_id, $request->senha)){

				$msgs['chat'] = array_merge(
					json_decode(\App\tbl_suportechat::where('enviador_id', $request->enviador_id)->get()), 
					json_decode(\App\tbl_suportechat::where('receptor_id', $request->enviador_id)->get())
					);
				
				$msgs['status'] = 200;
			}
			return json_encode($msgs);
		}	
	}

}
