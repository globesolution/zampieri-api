<?php 
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;

use Illuminate\Http\Request;

class usuariosController extends Controller {

	public function	auth(Request $request)
	{

		$rules = array(
    		'email'    => 'required|email',
    		'senha'    => 'required|alphaNum|min:3'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			$auth = \App\tbl_usuarios::select(
				'id',
				'nome_completo',
				'email',
				'senha',
				'saldo',
				'type_user'
				)
			->where('email', $request->email)
			->where('senha', $request->senha);

			if($auth->count() == 1){
				$retorno['usuario'] = $auth->get()[0];
				
				$retorno['msgnovidade'] = \App\tbl_msgnovidades::where('status', 1)->where('tipo_msg', 0)->first()->msgNovidade;
				if($auth->get()[0]['type_user'] == 1){

					
					$pgt = \App\tbl_pagamentos::select('valor_pagamento', 'created_at')->where('usuario_id', $auth->get()[0]['id'])->where('status', 2)->get();
					


					for ($i=0; $i < count($pgt) ; $i++) { 
						$pgt[$i]['tipo_data'] = 1;
					}
					$ext = \App\tbl_extrato::select('valor as valor_pagamento', 'created_at')->where('usuario_id', $auth->get()[0]['id'])->where('tipo_entrada', 2)->get();
					

					for ($i=0; $i < count($ext) ; $i++) { 
						$ext[$i]['tipo_data'] = 0; 
					}

					
					$retorno['msgpush'] = array_merge(json_decode($ext),json_decode($pgt));

				}else if($auth->get()[0]['type_user'] == 2){

					$retorno['dash']['indicacoes']['concluidas'] = count(\App\tbl_indicacoesInquilinos::where('status', 2)->get())+count(\App\tbl_indicacoesImoveis::where('status', 2)->get());
					$retorno['dash']['indicacoes']['abertas'] = count(\App\tbl_indicacoesInquilinos::where('status', 1)->get())+count(\App\tbl_indicacoesImoveis::where('status', 1)->get());
					$retorno['dash']['indicacoes']['canceladas'] = count(\App\tbl_indicacoesInquilinos::where('status', 3)->get())+count(\App\tbl_indicacoesImoveis::where('status', 3)->get());
					
					$imoveis = \App\tbl_indicacoesImoveis::select(
								'nome_indicado',
								'fone_1',
								'created_at',
								'tipo_indicacao',
								'status'
							)->get();

					$inquilinos = \App\tbl_indicacoesInquilinos::select(
							'nome_indicado',
							'fone_1',
							'created_at',
							'tipo_indicacao',
							'status'
						)->get();


					$data = array_merge(json_decode($imoveis, true),json_decode($inquilinos, true));
					
					usort($data, function ( $a, $b ) {
    					return strtotime($a["created_at"]) - strtotime($b["created_at"]);
					});



					$retorno['dash']['indicacoes']['lista'] = array_slice($data, 0, 10);
				}

				$retorno['status'] = 200;
				
				return json_encode($retorno);
			}else{
				$error = array(
					'status' => 404,
					'msg'    => 'Usuário ou senha incorretos.'
				);
				return json_encode($error);
			}

		}
	}


	public function createNewUser(Request $request)
	{

		$rules = array(
    		'nome_completo'    => 'required|string|min:3',
    		'email'            => 'required|email',
    		'senha'            => 'required|alphaNum|min:3',
    		'fone_1'           => 'required|numeric|min:10'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{

			$verificaSeExiste = \App\tbl_usuarios::where('email', $request->email)
				->get();

			if(count($verificaSeExiste) == 0)
			{
				$newUser = new \App\tbl_usuarios;
				$newUser->nome_completo = $request->nome_completo;
				$newUser->email = $request->email;
				$newUser->senha = $request->senha;
				$newUser->fone_1 = $request->fone_1;
				$newUser->fone_2 = $request->fone_2;
				$newUser->saldo = 0;
				$newUser->type_user = 1;
				$newUser->status = 1;
				//status 1 = usuario comum
				//status 2 = usuario admin
				$newUser->save();


				$client = new \GuzzleHttp\Client; 
				$client->setDefaultOption('verify', false);
				$response = $client->post('https://www.rdstation.com.br/api/1.3/conversions',
                	['body' => 
                		[
                    	"token_rdstation" => "73748086b8af689e984d8fb11304cc51",
                    	"identificador"   => "cadastro-usuario-aplicativo",
                    	"email"			  => $request->email,
	                    "nome" 			  => $request->nome_completo,
	                    "telefone"		  => $request->fone_1,
	                    "celular"		  => $request->fone_2,
	                    "tags"			  => "savo-app"
	                    ]
	                ]
	            );


				$success = array(
					'status' => 200,
					'msg'    => 'Cadastro realizado com sucesso.'
				);
				return json_encode($success);


			}else{
				$error = array(
					'status' => 404,
					'msg'    => 'Esses dados já constam no nosso banco de dados.'
				);

	    		return json_encode($error);

			}
			
			
		}
	}


	//criar nova indicação de inquilino
	public function createNewIndicacaoInquilino(Request $request)
	{
		$rules = array(
    		'nome_indicado'    => 'required|string|min:3',
    		'email_indicado'   => 'required|email',
    		'fone_1'           => 'required|numeric|min:8'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			$indicacao = new \App\tbl_indicacoesInquilinos;
			$indicacao->usuario_id = $request->usuario_id;
			$indicacao->nome_indicado = $request->nome_indicado;
			$indicacao->email_indicado = $request->email_indicado;
			$indicacao->fone_1 = $request->fone_1;
			$indicacao->fone_2 = $request->fone_2;
			$indicacao->observacoes = $request->observacoes;
			$indicacao->tipo_indicacao = 1;
			$indicacao->status = 1;
			$indicacao->save();

			// status 1 amarelo = em avaliação
			// status 2 verde = concluido / saldo creditado
			// status 3 cancelado = algo deu errado


			$client = new \GuzzleHttp\Client; 
			$client->setDefaultOption('verify', false);
			$response = $client->post('https://www.rdstation.com.br/api/1.3/conversions',
            	['body' => 
            		[
                	"token_rdstation" => "73748086b8af689e984d8fb11304cc51",
                	"identificador"   => "cadastro-inquilino-aplicativo",
                	"email"			  => $request->email_indicado,
                    "nome" 			  => $request->nome_completo,
                    "telefone"		  => $request->fone_1,
                    "celular"		  => $request->fone_2,
                    "tags"			  => "savo-app"
                    ]
                ]
            ); 

			$success = array(
					'status' => 200,
					'msg'    => 'Cadastro realizado com sucesso.'
				);
			return json_encode($success);
		}	
	}

	//criar nova indicação de imovel
	public function createNewIndicacaoImovel(Request $request)
	{
		$rules = array(
    		'nome_indicado'       => 'required|string|min:3',
    		'fone_1'              => 'required|numeric|min:10',
    		'endereco'            => 'required|string|min:4',
    		'email_indicado'   	  => 'required|email',
    		'bairro'              => 'required|string|min:3'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			$indicacao = new \App\tbl_indicacoesImoveis;
			$indicacao->usuario_id = $request->usuario_id;
			$indicacao->nome_indicado = $request->nome_indicado;
			$indicacao->email_indicado = $request->email_indicado;
			$indicacao->fone_1 = $request->fone_1;
			$indicacao->fone_2 = $request->fone_2;
			$indicacao->endereco = $request->endereco;
			$indicacao->cep = $request->cep;
			$indicacao->bairro = $request->bairro;
			$indicacao->numero = $request->numero;
			$indicacao->complemento = $request->complemento;
			$indicacao->observacoes = $request->observacoes;
			$indicacao->tipo_indicacao = 2;
			$indicacao->status = 1;
			$indicacao->save();
			// status 1 amarelo = em avaliação
			// status 2 verde = concluido / saldo creditado
			// status 3 cancelado = algo deu errado


			$client = new \GuzzleHttp\Client; 
			$client->setDefaultOption('verify', false);
			$response = $client->post('https://www.rdstation.com.br/api/1.3/conversions',
            	['body' => 
            		[
                	"token_rdstation" => "73748086b8af689e984d8fb11304cc51",
                	"identificador"   => "cadastro-imovel-aplicativo",
                	"email"			  => $request->email_indicado,
                    "nome" 			  => $request->nome_completo,
                    "telefone"		  => $request->fone_1,
                    "celular"		  => $request->fone_2,
                    "tags"			  => "savo-app"
                    ]
                ]
            );

			$success = array(
					'status' => 200,
					'msg'    => 'Cadastro realizado com sucesso.'
				);
			return json_encode($success);
		}
	}




	public function getIndicacoes(Request $request)
	{
		$imoveis = \App\tbl_indicacoesImoveis::select(
				'nome_indicado',
				'fone_1',
				'created_at',
				'tipo_indicacao',
				'status'
			)
		->where('usuario_id', $request->usuario_id)->get();

		$inquilinos = \App\tbl_indicacoesInquilinos::select(
				'nome_indicado',
				'fone_1',
				'created_at',
				'tipo_indicacao',
				'status'
			)
		->where('usuario_id', $request->usuario_id)->get();

		
		$retorno['status'] = 200;
		$retorno['indicacoes'] = array_merge(json_decode($imoveis),json_decode($inquilinos));
		return $retorno;

	}


	public function solicitarPagamento(Request $request){

		$rules = array(
    		'usuario_id'   		 => 'required',
    		'senha'              => 'required|min:3',
    		'valor_pagamento'    => 'required',
    		'numero_conta'       => 'required|min:3',
    		'numero_agencia'     => 'required|min:3',
    		'nome_banco'         => 'required|min:3',
    		'cpf'                => 'required|numeric|min:9'
		);

		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			$userDebitado = \App\tbl_usuarios::where('id', $request->usuario_id)->where('senha', $request->senha)->first();
			if(count($userDebitado) != 1){
				$error = array(
					'status' => 404,
					'msg'    => 'Senha incorreta.'
				);

    			return json_encode($error);
			}else{
				if($userDebitado->saldo < $request->valor_pagamento){
					$error = array(
						'status' => 404,
						'msg'    => 'Saldo insuficiente.'
					);
					return json_encode($error);
				}else{
					
					$userDebitado->saldo = $userDebitado->saldo - $request->valor_pagamento;


					$pagamento = new \App\tbl_pagamentos;
					$pagamento->usuario_id = $request->usuario_id;
					$pagamento->data_pedido = date("Y-m-d H:i:s");
					$pagamento->valor_pagamento = $request->valor_pagamento;
					$pagamento->numero_agencia = $request->numero_agencia;
					$pagamento->numero_conta = $request->numero_conta;
					$pagamento->nome_banco = $request->nome_banco;
					$pagamento->cpf = $request->cpf;
					$pagamento->status = 1;
					//status pagamento 1 = não pago
					//status pagamento 2 =  pago

					$extrato = new \App\tbl_extrato;
					$extrato->usuario_id = $request->usuario_id;
					$extrato->tipo_entrada = 1;
					//tipo entrada 1 = debitado da conta
					//tipo entrada 2 = creditado na conta
					$extrato->valor = $request->valor_pagamento;
					
					$extrato->save();
					$pagamento->save();
					$userDebitado->save();


					$success = array(
						'status' => 200,
						'msg'    => 'Pedido realizado com sucesso.'
					);
					return json_encode($success);

				}
			}

		}
	}

	public function getExtrato(Request $request){
		$rules = array(
    		'usuario_id'   		 => 'required|numeric|min:1'
		);

		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			
			$extratoUser['extrato'] = \App\tbl_extrato::where('usuario_id', $request->usuario_id)->orderBy('id', 'DESC')->get();
			$extratoUser['status']= 200;
			return $extratoUser;

		}


	}

	public function getInfos(Request $request){
		$rules = array(
    		'usuario_id'   		 => 'required|numeric|min:1'
		);

		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			
			$infos['infos']['imoveis'] = count(\App\tbl_indicacoesImoveis::where('usuario_id', $request->usuario_id)->orderBy('id', 'DESC')->get());
			$infos['infos']['inquilinos'] = count(\App\tbl_indicacoesInquilinos::where('usuario_id', $request->usuario_id)->orderBy('id', 'DESC')->get());
			
			$infos['status']= 200;
			return $infos;

		}


	}



	public function recoveryPwd(Request $request){
		$rules = array(
    		'email'   		 => 'required|email'
		);

		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{

			$password = \App\tbl_usuarios::select('senha')->where('email', $request->email)->first();
			

			if (count($password) == 1){
				$msg = "Sua senha é: ".$password->senha;

				mail($request->email,"ZAMPIERI - SUA SENHA", $msg);
				$success = array(
					'status' => 200,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

	    		return json_encode($success);
			}else{
				$error = array(
					'status' => 404,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

	    		return json_encode($error);
			}	
		}
	}


	public function changePassword(Request $request){
		$rules = array(
    		'usuario_id'   		 => 'required|numeric|min:1',
    		'senha_old'   		 => 'required',
    		'senha_new'   		 => 'required',
    		'senha_new2'   		 => 'required'
		);

		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{

			$changePassword = \App\tbl_usuarios::where('id', $request->usuario_id)->where('senha', $request->senha_old)->first();
				
			

			if (count($changePassword) == 1){
					
				if($request->senha_new == $request->senha_new2){
					$changePassword->senha = $request->senha_new;
					$changePassword->save();
					$success = array(
						'status' => 200,
						'msg'    => 'Alterado com sucesso'
					);

	    			return json_encode($success);

				}else{
					$error = array(
						'status' => 200,
						'msg'    => 'Senhas não conferem'
					);

	    			return json_encode($error);

				}

			}else{
				$error = array(
					'status' => 404,
					'msg'    => 'Parece que algo está errado, tente novamente'
				);

	    		return json_encode($error);
			}	
		}
	}


	public function sendNewMsg(Request $request)
	{
		$rules = array(
    		'usuario_id'    => 'required|numeric|min:1',
    		'msg'           => 'required'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			
			$msg = new \App\tbl_suportechat;
			$msg->msg = $request->msg;
			$msg->enviador_id = $request->usuario_id;
			$msg->receptor_id = -9; //id to admin
			$msg->tipo_user = 1;
			$msg->lida = 1;
			$msg->save();
			$success = array(
					'status' => 200,
					'msg'    => 'Cadastro realizado com sucesso.'
				);
			return json_encode($success);
		}	
	}

	public function getMsgs(Request $request)
	{
		$rules = array(
    		'usuario_id'    => 'required|numeric|min:1'
		);
		$validator = \Validator::make(\Input::all(), $rules);
		if ($validator->fails()){
			$error = array(
				'status' => 404,
				'msg'    => 'Parece que algo está errado, tente novamente'
			);

    		return json_encode($error);
		}else{
			
			$msgs['msgs'] = \App\tbl_suportechat::where('enviador_id', $request->usuario_id)->orWhere('receptor_id', $request->usuario_id)->get();
			$msgs['status'] = 200;

			return json_encode($msgs);
		}	
	}

}
