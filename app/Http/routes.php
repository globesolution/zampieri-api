<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//auth user
Route::post('/api/auth', 'usuariosController@auth');




//create user
Route::post('/api/usuario/new/user', 'usuariosController@createNewUser');

//criar indicaoes
Route::post('/api/usuario/new/indicacao/imovel', 'usuariosController@createNewIndicacaoImovel');
Route::post('/api/usuario/new/indicacao/inquilino', 'usuariosController@createNewIndicacaoInquilino');

//listar indicacoes sem detalhamento
Route::post('/api/usuario/list/indicacao', 'usuariosController@getIndicacoes');
//solicitar pagamento
Route::post('/api/usuario/new/pagamento', 'usuariosController@solicitarPagamento');




//get extrato
Route::post('/api/usuario/extrato', 'usuariosController@getExtrato');

Route::post('/api/usuario/infos', 'usuariosController@getInfos');
Route::post('/api/usuario/recovery', 'usuariosController@recoveryPwd');
Route::post('/api/usuario/changepassword', 'usuariosController@changePassword');
Route::post('/api/usuario/send/msg', 'usuariosController@sendNewMsg');
Route::post('/api/usuario/get/msg', 'usuariosController@getMsgs');

//funcçoes admin





Route::post('/api/admin/list/pagamentos', 'adminController@listarPagamentos');
Route::post('/api/admin/save/pagamento', 'adminController@salvarPagamentos');


Route::post('/api/admin/list/indicacoes', 'adminController@listarIndicacoes');
Route::post('/api/admin/save/indicacao', 'adminController@salvarIndicacao');

Route::post('/api/admin/number/indicacoes', 'adminController@numberIndicacoes');
Route::post('/api/admin/msg/novidades', 'adminController@newMsgNovidade');
Route::post('/api/admin/getlist/msg', 'adminController@getMsgsToList');

Route::post('/api/admin/getbysender/msg', 'adminController@getMsgsBySender');

Route::post('/api/admin/send/msg', 'adminController@sendNewMsg');



